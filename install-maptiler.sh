### This is approximately a transcript of how I installed maptiler on wifaz.de
### Please handle with care. It serves for documentary purposes.
### It may or may not be appropriate to install it again or elsewhere.

## install nodejs 10 
# see https://blog.eldernode.com/install-node-js-on-debian-10/
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt-get install -y nodejs libgles2

## install maptiler stuff as collected by create-maptiler-tar.sh
cd /openhitch
tar xvfz /root/maptiler.tgz

## create dedicated user 
adduser --system --home /openhitch/maptiler maptiler
chown -R maptiler.root maptiler

## install tileserver-gl useing npm
cd maptiler
sudo -u maptiler npm install tileserver-gl

## nginx configuration
cat > /etc/nginx/conf.d/maptiler.conf <<'XXXXX'
server {
    server_name wifaz.de;
    listen 33470 ssl;
    location / {
        proxy_pass http://127.0.0.1:8080;
        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto  https;  # $http_x_forwarded_proto;
        proxy_set_header Host               $http_host;
    }
    ssl_certificate /etc/letsencrypt/live/wifaz.de/fullchain.pem; # managed by Certbot

    ssl_certificate_key /etc/letsencrypt/live/wifaz.de/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}
XXXXX

## create maptiler service
cat > /etc/systemd/system/maptiler.service <<'YYYYY'
[Unit]
Description=tileserver-gl service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
Usercat > /etc/systemd/system/maptiler.service <<YYYYY
[Unit]
Description=tileserver-gl service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=maptiler
ExecStart=/usr/bin/npx tileserver-gl -c /openhitch/maptiler/config.json

[Install]
WantedBy=multi-user.target

service maptiler start
service nginx restart
