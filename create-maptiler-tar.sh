### This script for extracts maptiler assets from a running docker container
### and packages them together with configuration into a tar file
### Before running this script, a openmaptiles docker container must be run and installed as discribed
### here: https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch/-/blob/master/Setup.md

## Retrieve container id
mkdir maptiler
cd maptiler
export CID=`docker ps | grep klokantech/openmaptiles-server | cut -d' ' -f1`
mkdir data
docker cp $CID:/data/osm-2020-03-16-v3.11-germany_berlin.mbtiles data/
docker cp $CID:/usr/local/src/server_frontpage_copy.tmpl .
docker cp $CID:/usr/local/src/wizard/assets/watermarkdecor.js .
mkdir styles
docker cp $CID:/usr/local/src/assets/styles/osm-bright/ styles/
docker cp $CID:/usr/local/src/assets/fonts .

cat > config.json <<'EOF'
{
  "options": {
    "paths": {
      "fonts": "/openhitch/maptiler/fonts",
      "styles": "/openhitch/maptiler/styles",
      "mbtiles": "/openhitch/maptiler/data"
    },
    "dataDecorator": "/openhitch/maptiler/watermarkdecor.js",
    "domains": [],
    "frontPage": "/openhitch/maptiler/server_frontpage_copy.tmpl",
    "maxScaleFactor": 2,
    "maxSize": 2048,
    "serveAllFonts": true
  },
  "styles": {
    "osm-bright": {
      "style": "osm-bright/style-local.json",
      "serve_data": true,
      "tilejson": {
        "bounds": [
          12.26,
          51.849,
          14.699,
          52.994
        ],
        "format": "png"
      }
    }
  },
  "data": {
    "v3": {
      "mbtiles": "osm-2020-03-16-v3.11-germany_berlin.mbtiles"
    }
  }
}
EOF

cd ..
tar cvfz maptiler.tgz maptiler
rm -r maptiler
