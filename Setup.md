# Setting up a development environment

## IDE
* Install Intellij, community edition is sufficient.
  * In Intellij, go to Settings &rarr; Build, Execution, Deployment &rarr; Compiler &rarr; Kotlin Compiler
  * set Target JVM version to 1.8
* Install Android SDK
  - Either install Android Studio
  - Or install Android SDK without Android Studio. On linux you might follow [Enriques instructions](https://dev.to/enriquem/android-sdk-without-studio-3idg) 
  
  Note the path to the Android Sdk. On linux, if you installed Android Studio, it is probably $HOME/Android/Sdk or if you followed
  Enriques instructions, it is $HOME/.android-sdk
* Install Git
* Install OpenJDK 8
* Install Maven

## Repositories
* Clone some repositories from https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch, i.e.
  ```
  git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch-protocol.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/graphhopper-openhitch.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch-android.git
  ```

## Please Help
Open openhitch/Setup.md in a text editor and note all difficulties you have and how you dealt with them in the section [Annotations](#Annotations). Upload when you are done

## Initial build
To make sure that everything is in a working state, build all projects in the required order. This will take a while. If you'd rather do this in your IDE, skip this section an proceed with the next one

```
cd openhitch-protocol
mvn install -T 1C -DskipTests
cd ../graphhopper-openhitch
mvn install -T 1C -DskipTests
```

## Prepare and start the Openhitch Server
You will need a Graphhopper map file to be used by the backend. Download https://download.geofabrik.de/europe/germany/berlin-200101.osm.pbf rename it to europe_germany_berlin.pbf and copy it to graphhopper-openhitch. The graphhopper-openhitch directory should look something like this:
```
$ ls graphhopper-openhitch
LICENSE.txt  config.yml  europe_germany_berlin.pbf  gh-data  logs  pom.xml  src  target
```

Now you are ready to run the Graphhopper backend ‒ congratulations! If you'd rather start this form your IDE, see below.
```
cd graphhopper-openhitch
mvn exec:java
```

You should see some log output:
```
2020-10-13 20:32:44.593 [de.wifaz.oh.server.OpenHitchApplication.main()] INFO  o.e.jetty.server.AbstractConnector - Started application@22551405{HTTP/1.1, (http/1.1)}{localhost:8989}
2020-10-13 20:32:44.600 [de.wifaz.oh.server.OpenHitchApplication.main()] INFO  o.e.jetty.server.AbstractConnector - Started admin@2a3af34f{HTTP/1.1, (http/1.1)}{localhost:8990}
2020-10-13 20:32:44.600 [de.wifaz.oh.server.OpenHitchApplication.main()] INFO  org.eclipse.jetty.server.Server - Started @4130ms
```

Now you can open http://localhost:8989 in your browser and should see the Graphhopper app. 

Close the server again by pressing Ctrl-C, as we will start it from the IDE later.

## Setup IntelliJ

### Protocol Definition
* Open Intelij
* Open Project openhitch-protocol
* Specify java 1.8 as project SDK (right click project &rarr; Open Module Settings &rarr; Modules &rarr; Dependencies)
* Open Maven Tool Window by clicking the Maven-Button at the right edge of the IDE and doubleclick "OpenHitch Protocol &rarr; Lifecycle &rarr; install"
  (alternatively start `mvn install` in a terminal)
  This will install the protocol definition in the local Maven central.

### GraphHopper/OpenHitch Server 
* With Intellij open project graphhopper-openhitch
* Click "Build &rarr; Build Project"
  Do update Kotlin compiler, if being asked).
* Click "Run &rarr; Edit Configurations"
* Click "+" to add new configuration
  * A menu pops up, choose Application
  * Fill the following values into the form
    * Name: openhitch-server
    * Main class: de.wifaz.oh.server.OpenHitchApplication
    * Program arguments: server config.yml
    * Working directory: /xyz/graphhopper-openhitch (replace xyz by the path where your graphhopper-openhitch directory is)
    * Use classpath of module: graphhopper-openhitch
    * JRE: 1.8
    * Keep defaults for all fields not mentioned here
* Click "Run &rarr; openhitch-server"

## App
Note: This instruction is for Intelij, but Android Studio can be used as well

* In intellij open project openhitch-android
* A gradle update should start automatically. If not so, open the Gradle Window by clicking the gradle button at the right edge of the IDE and there clicking the button with the reload symbol.
* During update, you will be prompted for the path to the Android SDK. Navigate to the path entered above.
* Go to File-->Project Structure-->Project SDK, choose 1.8
* Go to File-->Project Structure-->Platform Settings, choose Build target Android API 29 and Java SDK 1.8
* Click "Build &rarr; Build Project"
* Open "Tools &rarr; Android &rarr; AVD Manager" and configure a device with API 28 or 29
  (In theory we support all api levels starting from 16, but this is not yet tested)
* Start device, open extended controls by clicking the three dots in the toolbar for the device
    * set location to some point in Berlin, Germany 
    * go to Settings --> Advanced, Set `OpenGL ES Renderer` to `SwiftShader` and `OpenGL ES API level` to `Renderer maximum`
      (otherwise you may see black icons on the maps)
* Make shure the Wifi is activated.
* The language of the virtual device may be set to Englisch or German.
* Close device
* Duplicate the device (Click "Duplicate" in it's drop down menu in the rightmost column of the AVD Manager).
* At the top of the main window of Android Studio, there is a drop down list of devices. You can open it and click "Run on multiple devices" to install and run the app on both devices simultaneously.
* Now you can register on both smartphones and try to arrange a carpool.
* Alternatively, you can use the OpenHitch test database manager at http://localhost:8989/debug/dbm and read database "personas.json" into the server.
  Personas are test accounts that can be used without registration in the developer version. Click on the small blue person icon at the bottom of the
  registration screen or the bottom of the main menu, and select a persona.
  Other test databases additionally contain some ride offers and requests.

# Optional: Local Servers

By default, the developer configuration uses a local graphhopper/openhitch-server (the part we are developing)
but central servers for Maptiler and MQTT (the parts we just use as they are), thus making developer setup easier.
If you want to have everything at home and under control, you can also run Maptiler and MQTT locally, as below.

(For geocoding search, the openhitch server uses the Photon server provided by komoot.io .
Installing Photon locally and/or on wifaz.de is a task yet to be done)

## Local OpenMapTiles (Map server)
Install and start the OpenMapTiles docker container:

`sudo docker run --rm -it -v $(pwd):/data -p 8080:80 klokantech/openmaptiles-server`

If you are using Docker Toolbox with Windows, you will need pass an absolute path to a directory in your home directory and the paths will have to start with two slashes, e.g.

`docker run --rm -it -v //c/Users/<your username>/openhitch/data://data -p 8080:80 klokantech/openmaptiles-server`

Complete the OpenMapTiles installation wizard
* point browser to http://localhost:8080 OpenMapTiles installation wizard should show up
* Go through wizard, choose
  * region: Germany/Berlin
  * style: OSM Bright
  * settings: Serve vector map tiles
  * Follow instructions on how to get a key and download the data

When you map server is running, you can let the app use it, by editing `developer-config.xml`
and using the map style urls given under "Alternative settings for local map server"


## Local Mosquitto (MQTT broker)
Run the docker Mosquitto container:

`docker run -d -p 1883:1883 --name mqtt eclipse-mosquitto`

Alternatively, you can install Mosquitto locally, e.g. on debian using `sudo apt install mosquitto`

To use the local Mosquitto server, do
* In the server project: Edit `config.yml` and set `openhitch.mqtt.server` to `tcp://localhost:1883`
* In the app project: Edit `developer-config.xml` and set `mqtt_server_uri` to `tcp://10.0.2.2:1883`
  (The android emulator routes 10.0.2.2 to the host machine. localhost or 127.0.0.1 is wrong here, since for the app it is the emulated smartphone)

# Annotations
* When running the maptiler server locally: If the setup page for openmaptiles-server cannot be reached under http://localhost:8080,
try getting the docker container ip with `docker-machine ip default` and then open
http://<docker-container-ip>:8080

* When adding the graphhopper-openhitch module to the graphhopper project, it might be necessary to install the
openhitch-protocol with the same com.fasterxml.jackson.core version as used in the graphhopper project

## Obsolete Annotations
The following annotations should be obsolete now, but are kept for reference, in case somebody wants to use older versions or
issues pop up again

* The graphhopper project might already contain the navigation module (Yes it does, instructions to add it as a module are now deleted)

* To build geocoder-converter, the version of graphhopper-core in the pom might need to
be updated to the version in the graphhopper project (versions should match now, if you check out the newest master of  graphhopper-core and geocoder-converter)



