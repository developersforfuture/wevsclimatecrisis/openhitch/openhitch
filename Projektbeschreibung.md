# Projekt OpenHitch, worum geht es?

Diese Seite beschreibt Motivation und Inhalt des Projektes, für eine technische Übersicht siehe [README.md](README.md)

## Problem
Eines der größten Klimaprobleme ist der PKW-Verkehr. In den meisten PKW sitzt nur eine Person, die durchschnittliche
Besetzungszahl ist 1,3. Allein in Deutschland werden jährlich ca 70 Mio t CO ausgetoßen und mehr als 100 Mio € dafür ausgegeben
leere PKW-Sitze zu bewegen. Es ist eine unfassbare Verschwendung. Daran hat sich seit 1990 nichts geändert.

Wir können das ändern. Wenn irgendo ein Auto durch die Gegend fährt, ist es fast leer, und fast immer fährt ein ebenso leeres
Auto in die gleiche Richtung. Wenn wir es hinbekommen, dass diese Leute zusammen ein Auto verwenden, ist der halbe
CO2-Ausstoß eingespart.


## Idee
eine App, die das vermittelt. Für Fahrer:innen verhält sie sich wie ein Navi. Einziger Unterschied: manchmal sagt sie
dass man jetzt rechts ranfahren, da steht jemand und will mitfahren. Mitfahrer:innen trampen per Handy, die App hält ihnen die Autos an.
Im Hintergrund läuft ein Bezahlsystem, das vollautomatisch eine Unkostenbeteiligung von den Mitfahrer:innen an die Fahrer:innen fließen läßt.
Wenn genügend Leute mitmachen, entsteht ein sicheres, bequemes, allgegenwärtiges Transportsystem, das halb so teuer ist und
halb soviel CO2 ausstößt wie bisher.

Das schöne an der Idee ist, wir brauchen keine aufwendige neue Technik, wir müssen nicht auf die Regierung warten und
wir können trotzdem einen Haufen CO2 einsparen.

## Kritische Masse
Die Idee ist nicht neu und wurde schon mehrfach versucht. Das große Problem ist es, die kritische Masse zu erreichen.
Wenn nicht geügend Leute mitmachen, funktioniert das System nicht. Keine Firma hat es geschafft, diese
Hürde zu überwinden.
Aber als Bürgerbewegung werden wir es schaffen. Die For-Future-Bewegung ist die ideale Plattform um die kritische Masse zusammenzubekommen.


## OpenHitch
Das Projekt OpenHitch ist es, die oben beschriebene App und die zugehörige Serverlandschaft zu entwickeln.
Die App wird zunächst für Android entwickelt, andere Plattformen sollen folgen.
Zusätzlich zu dem oben beschiebenen beinhaltet die App auch Vorausplanungs-Features. Wenn es gut läuft,
werden die vielleicht irgendwann nicht mehr gebraucht, aber für eine Startphase ist das sehr wichtig.

### Ziele
* **Open Source**: Alle Komponenten des Systems sollen Open Source sein, um langfristig zu gewährleisten, dass wir das System unabhängig von
  Autoindustrie und Internet-Monopolisten betreiben können. Die Client-Software läuft unter Apache License, die Server-Software unter SSPL
  (wird von vielen nicht als Open Source gewertet, verlangt aber vom Betreiber die maximale Transparenz)
* Unabhängigkeit von Google Play
* Auf möglichst alten Handys lauffähig (wir wollen ja nicht, dass sich jemand dafür ein neues Handy anschaffen muss)

### Technische Komponenten
## Programmiersprache
* Kotlin (App und Server)
## Server
* Routenplaner: Graphhopper
* Map Server: OpenMapTiles
* Geocoding Suche: Photon
* Matching und Fahrtvermittlung: Selbst geschrieben
## App
* MapBox (die Verisionen, die noch Open Source sind)
* ReactiveX (RxKotlin)
## Kommunikation
* HTTP: Jersey, Retrofit
* MQTT: Mosquitto, Paho

### Mitstreiter gesucht

Wir kommunizieren über den Mattermost von Developers For Future, Gruppe [Projec: OpenHitch](https://mattermost.developersforfuture.org/developers/channels/project-openhitch)

[Hier](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch) gibt es eine Anleitung, wie man eine
Entwicklungsumgebung zum laufen bringt.

Wir treffen uns jeden Dienstag abend um 19:30 zu einer Videkonferenz
Bitte in der Mattermost-Gruppe gucken wo oder max@knirz.de fragen.
Da kann man gerne mal vorbeikommen.

Außerdem bin ich (Max) fast immer für eine Einführussession oder Remote Pairing zu haben,
einfach Terminvorschlag auf Mattermost oder an max@knirz.de schicken.
