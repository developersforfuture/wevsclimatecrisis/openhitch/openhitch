# OpenHitch

OpenHitch is an ongoing effort to produce an open source realtime carpooling system.
For a more comprehensive project description in german see [Projektbeschreibung.md](Projektbeschreibung.md)


This repository is just an umbrella for hosting meta-information, like
the [issue-board](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch/-/boards/2044601?milestone_title=MVP&)
and the [wiki](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch/-/wikis/home)

The code resides in three repositories:

* [openhitch-protocol](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch-protocol): Classes defining the protocol
* [graphhopper-openhitch](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/graphhopper-openhitch): The server
* [openhitch-android](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch-android): The android app



We are a small subgroup of DefelopersForFuture. Further help is highly appreciated.

For instructions on how to set up a development environment, see https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch/-/blob/master/Setup.md

Meet us on mattermost channel https://mattermost.developersforfuture.org/developers/channels/project-openhitch

Please refer to max@knirz.de for invitations.
